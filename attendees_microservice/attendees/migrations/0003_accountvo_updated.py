# Generated by Django 4.0.3 on 2023-05-17 21:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attendees', '0002_accountvo'),
    ]

    operations = [
        migrations.AddField(
            model_name='accountvo',
            name='updated',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
